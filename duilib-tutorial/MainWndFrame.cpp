#include "pch.h"
#include "MainWndFrame.h"
#include "Resource.h"

DUI_BEGIN_MESSAGE_MAP(MainWndFrame, CNotifyPump)
	DUI_ON_MSGTYPE(DUI_MSGTYPE_CLICK, OnClick)
DUI_END_MESSAGE_MAP()

const LPCTSTR MainWndFrame::kClassName = _T("main_wnd_frame");
const LPCTSTR MainWndFrame::kMainWndFrame = _T("main_wnd_frame.xml");

DuiLib::CDuiString MainWndFrame::GetSkinFolder()
{
	// GetInstancePath 接口返回默认的皮肤文件位置
	// 在 main 函数中我们可以通过 SetResourcePath 来设置路径
	return m_PaintManager.GetInstancePath();
}

DuiLib::CDuiString MainWndFrame::GetSkinFile()
{
	// 成员变量定义的皮肤文件名
	return kMainWndFrame;
}

LPCTSTR MainWndFrame::GetWindowClassName(void) const
{
	// 成员变量定义的窗口 class name
	return kClassName;
}

DuiLib::UILIB_RESOURCETYPE MainWndFrame::GetResourceType() const
{
#if _DEBUG
	return UILIB_RESOURCETYPE::UILIB_FILE;
#else
	return UILIB_RESOURCETYPE::UILIB_ZIPRESOURCE;
#endif
}

LPCTSTR MainWndFrame::GetResourceID() const
{
	return MAKEINTRESOURCE(IDR_ZIPRES1);
}


void MainWndFrame::InitWindow()
{
#if 0
	// 使用代码方式创建
	CButtonUI* btn = new CButtonUI;
	btn->SetText(_T("动态添加的按钮"));
	btn->SetBkColor(0xFFFFFFFF);
	btn->SetFixedHeight(60);
	btn->SetFixedWidth(100);

	m_pMainWndContent->Add(btn);
#endif
	//CControlUI* pControl = nullptr;
	//if (m_pBuilder.GetMarkup()->IsValid())
	//{
	//	pControl = m_pBuilder.Create(this, &m_PaintManager);
	//}
	//else
	//{
	//	pControl = m_pBuilder.Create(_T("list_item.xml"), (UINT)0, this, &m_PaintManager);
	//}
	//m_pMainWndList->Add(pControl);
}

void MainWndFrame::Notify(TNotifyUI& msg)
{
#if 0
	if (msg.sType == DUI_MSGTYPE_CLICK)
	{
		CDuiString strName = msg.pSender->GetName();
		if (strName == _T("btn_wnd_min"))
		{
			SendMessage(WM_SYSCOMMAND, SC_MINIMIZE, 0);
		}
	}
#endif
	__super::Notify(msg);
}

LRESULT MainWndFrame::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (uMsg == WM_CLOSE)
	{
		PostQuitMessage(0L);
	}

	return __super::OnClose(uMsg, wParam, lParam, bHandled);
}

void MainWndFrame::OnClick(TNotifyUI& msg)
{
	CDuiString strName = msg.pSender->GetName();
	if (strName == _T("aboutbtn"))
	{
		if (m_pAboutWndFrame == nullptr)
		{
			m_pAboutWndFrame = new AboutWndFrame();
			m_pAboutWndFrame->Create(this->GetHWND(), AboutWndFrame::kClassName, UI_WNDSTYLE_DIALOG, 0);
		}
		m_pAboutWndFrame->CenterWindow();
		m_pAboutWndFrame->ShowWindow();
	}

	__super::OnClick(msg);
}